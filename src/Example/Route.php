<?php
/**
 * More Usage: https://github.com/bramus/router
 */
$_ROUTER->get('/', function () {
    $ctrl = new example\controller\User();

    render('index.php', ['str' => $ctrl->showAtFrontend()]);
});
