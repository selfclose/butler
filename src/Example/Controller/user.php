<?php

namespace Example\controller;

class User extends \example\model\User {
    function showAtAPI() {
        json_success('Hello!!!');
    }

    public function showAtFrontend() {
        return 'This is text from Controller/User->showAtFrontend()';
    }
}
