<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Butler - <?=$GLOBALS['theme']['title']?></title>
</head>
<style>
    .panel {
        border: 1px solid #ccc;
        border-radius: 6px;
        padding: 2em;
        background: rgba(255, 255, 255, 0.2);
    }
    body {
        margin: 0;
        width: 100%;
        height: 100vh;
        font-family: sans-serif;
        color: #fff;
        background: linear-gradient(to bottom, rgb(10, 10, 50) 0%,rgb(60, 10, 60) 100%);
        vertical-align: middle;
    }
</style>
<body>
