<?php
if (!defined('__ROOT__')) die('Forbidden');

define('CONFIG', [
    'DB_ENABLE' => false, //this project use db?
    'DB_HOST' => 'localhost',
    'DB_USER' => 'root',
    'DB_PASS' => 'root',
    'DB_NAME' => 'heng_stats',
    'DB_PORT' => 3306,
    'DB_DEBUG' => false,
    'DB_FREEZE' => false, // freeze = lock database construct
    'Timezone' => 'Asia/Bangkok',
    'SRC' => 'Example', // Active Bundle folder for project (/src/{project}) | Read more in readme.MD (SRC)
    'Debug' => [
        'File' => true,
        'HTTP' => true,
    ],
]);
