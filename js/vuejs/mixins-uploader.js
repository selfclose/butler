let vueMixinsUploader = {
    data() {
        return {
            upload: {
                image: '',
            }
        }
    },
    methods: {
        //usage: <input type="file" @change="uploadImage($event, 'model for base64', 'model for filename')">
        uploadImage(el, modelBase64, modelFilename) {
            if (!modelBase64) return alert('Dev, Please define v-model');
            const image = el.target.files[0];
            let $this = this;
            const reader = new FileReader();
            reader.readAsDataURL(image);
            reader.onload = e =>{
                if (modelFilename) {
                    let f = el.target.value.replace(/^.*[\\\/]/, '');
                    eval(`$this.${modelFilename} = "${f}"`);
                }
                let base64 = e.target.result;
                eval(`$this.${modelBase64} = "${base64}"`);
            };
        },
        browseImage() {

        },
    }
};