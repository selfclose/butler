<?php
/**
 * For Extend by class, Same as sql class But this can declare $table and without to say $table again
 */
namespace selfclose\Butler;
use \R as R;
use sqlStatic as _sql;
abstract class sql {
    static $table;

    public $auto_timestamp = false; //automatic fill `created_at` and `updated_at` column at your db

    function query($query) {
        return _sql::query($query);
    }

    function insert($data) {
        if ($this->auto_timestamp) {
            $data['created_at'] = ymd();
            $data['updated_at'] = ymd();
        }
        return _sql::insert(static::$table, $data);
    }

    function update($data, $where) {
        if ($this->auto_timestamp) {
            $data['updated_at'] = ymd();
        }
        return _sql::update(static::$table, $data, $where);
    }

    function updateOrInsert($data, $where) {
        return _sql::updateOrInsert(static::$table, $data, $where);
    }

    function find($where = [], $col = '*', $limit = null, $order = null, $search = null) {
        return _sql::find(static::$table, $where, $col, $limit, $order, $search);
    }

    function findOne($where, $col = '*', $order = null, $search = null) {
        return _sql::findOne(static::$table, $where, $col, $order, $search = null);
    }

    function findOneOrInsert($where, $insertData) {
        return _sql::findOneOrInsert(static::$table, $where, $insertData);
    }

    function findPaginate($col = '*', $params = ['page' => 1, 'length' => 100, 'search' => null, 'filter' => null, 'where' => null, 'order_by' => ['id', false], 'group_by' => '', 'custom_total' => null], $join_query = null, $debug = false) {
        return _sql::findPaginate(static::$table, $col, $params, $join_query);
    }

    function delete($where) {
        return _sql::delete(static::$table, $where);
    }

    function count($col='*', $where = null, $returnNumber = false) {
        return _sql::count(static::$table, $col, $where, $returnNumber);
    }

    static function countUp($by = 1) {
        return 'update result set cnt = 0';
    }

    protected function countDown($by) {

    }

    function increment($where, $col, $by = 1) {
        return _sql::increment(static::$table, $where, $col, $by);
    }

    function decrement($where, $col, $by = 1) {
        return _sql::decrement(static::$table, $where, $col, $by);
    }

    function sum($col = '*', $where, $returnNumber = false) {
        return _sql::sum(static::$table, $col, $where, $returnNumber);
    }

    //----ETC----//
    static function isTableExist($tableName) {

    }

    static function resetIndex() {

    }

    static function findFromDateOffset($where = null, $dateOffset = 0, $column = 'created_at') {
        return _sql::findFromDateOffset(static::$table, $where, $dateOffset, $column);
    }
}
