<?php

class helper {
    static function safe_replace($string) {
        $string = str_replace('%20','',$string);
        $string = str_replace('%27','',$string);
        $string = str_replace('%2527','',$string);
        $string = str_replace('*','',$string);
        $string = str_replace('"','&quot;',$string);
        $string = str_replace("'",'',$string);
        $string = str_replace('"','',$string);
        $string = str_replace(';','',$string);
        $string = str_replace('<','&lt;',$string);
        $string = str_replace('>','&gt;',$string);
        $string = str_replace("{",'',$string);
        $string = str_replace('}','',$string);
        //$string = str_replace('\\','',$string);
        $string = str_replace('=','',$string);
        return $string;
    }

    static function ip()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_REAL_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_X_REAL_IP'])) {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (isset($_SERVER['HTTP_X_REAL_FORWARDED_FOR']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_X_REAL_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_REAL_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        return $ip;
    }

    static function is_email($email) {
        return strlen($email) > 6 && preg_match("/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/", $email);
    }

    function curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT,20);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,20);
        curl_setopt($ch, CURLOPT_USERAGENT, 'WEB_LIB_GI_81288128');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);
        if ($curl_errno > 0) {
            logData('cURL Error ('.$curl_errno.'):'. $curl_error.PHP_EOL.$url);
            return "http error";
        } else {
            return $output;
        }
    }

    function xml_to_array($xml)
    {
        $array = (array)(simplexml_load_string($xml));
        if(is_array($array) && isset($array['@attributes']))
        {
            return $array['@attributes'];
        }
        return array('info'=>'-100','msg'=>'error');
    }

    static function orderNumber($len=5)
    {
        $num = date('YmdHis').substr(microtime(), 2, $len).rand(0, 99);
        return $num;
    }

    static function checkRegex($txt, $regex) {

    }

    static function getUrl($withQueryString = false) {
        return strtok($_SERVER["REQUEST_URI"],$withQueryString ? '' : '?');
    }

    static function minify($str) {
        $expressions = array(
            'MULTILINE_COMMENT'     => '\Q/*\E[\s\S]+?\Q*/\E',
            'SINGLELINE_COMMENT'    => '(?:http|ftp)s?://(*SKIP)(*FAIL)|//.+',
            'WHITESPACE'            => '^\s+|\R\s*'
        );

        foreach ($expressions as $key => $expr) {
            $data = preg_replace('~'.$expr.'~m', '', $str);
        }
        return $str;
    }

    static function getDaysInMonth($month = null, $year = null) {
        return cal_days_in_month(CAL_GREGORIAN, $month ?: date('m'), $year ?: date('Y'));
    }

    static function base64ToFile($base64_string, $output_file) {
        $data = explode( ',', $base64_string );
        if (count($data) < 2) return false;
        $path = pathinfo($output_file);
        if (!file_exists($path['dirname'])) {
            mkdir($path['dirname'], 0777, true);
        }

        $ifp = fopen( $output_file, 'wb' );
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        fclose( $ifp );
        return $path;
    }

    static function randomString(int $length = 16, string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    static function sendFile($file, $download = false) {
        if (!file_exists($file)) return false;
        header('Content-Description: File Transfer');
        header("Content-type: ".mime_content_type($file)."; charset: UTF-8");
        if ($download) header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: '.filesize($file));
        readfile($file);
    }

    /**
     * Mr.ABC ASA --> M*******A
     */
    static function censorString($text, $padFront = 1, $padBack = 1, $censor = '*') {
        //get rid of thai Sara
        $text = preg_replace('/[\x{0E31}\x{0E33}-\x{0E3A}\x{0E47}-\x{0E4E}]/u', '', $text);
        $spl = self::str_split_unicode($text);
        $front = array_slice($spl, 0, $padFront);
        $back = array_slice($spl, -$padBack);
        $cnt = count($spl);
        return join($front).str_repeat($censor, $cnt/($padFront+$padBack)).join($back);
    }

    static function str_split_unicode($str, $length = 1) {
        $tmp = preg_split('~~u', $str, -1, PREG_SPLIT_NO_EMPTY);
        if ($length > 1) {
            $chunks = array_chunk($tmp, $length);
            foreach ($chunks as $i => $chunk) {
                $chunks[$i] = join('', (array) $chunk);
            }
            $tmp = $chunks;
        }
        return $tmp;
    }
}

class password {
    static function encrypt($password) {
        $seed = substr(md5(rand()), 0, 6);
        return $seed.'$'.md5($seed.$password);
    }

    static function compare($origin_password, $comparing_password) {
        $pass = explode('$', $origin_password);
        if (count($pass) != 2) return null;
        return md5($pass[0].$comparing_password) == $pass[1];
    }
}

class regex {
    static $__regs = [];

    // [Example] regex::addRegex('username', '/^[A-Za-z0-9-_]{6,20}$/', 'ชื่อผู้ใช้จะต้องเป็น A-Z หรือเลข ยาว 6 - 20 ตัว', 20);
    static function addRegex($name, $regex, $err_txt = '', $max_length = null) {
        global $__regex;
        self::$__regs[$name] = ['regex' => $regex, 'err' => $err_txt, 'length' => $max_length];
    }

    static function checkRegex($name, $check_txt, $json_error = true) {
        global $__regex;

        if (!self::$__regs[$name]) {
            if ($json_error) json_error('REGEX_NAME_NOT_EXIST');
            return false;
        }
        $reg = self::$__regs[$name];
        @preg_match($reg['regex'], $check_txt, $matches);
        if ($r_err = self::is_preg_error()) json_error('REGEX_ERROR|'.strtoupper($name), $r_err);

        if (!count($matches) && $json_error) {
            if ($json_error) json_error('FORMAT_ERROR|'.strtoupper($name), $reg['err']);
            return false;
        }
        return true;
    }

    static function getRegex($name, $trimHead = false, $asJson = false) {
        if (!self::$__regs[$name]) {
            if ($asJson) json_error('REGEX_NAME_NOT_EXIST');
            return false;
        }
//        $r = self::$__regs[$name]; //todo: make can delete /^ $/
//        if ($trimHead and substr($r['regex'], 0, 1) == '/^' and rtrim($r['regex'], '$/')) {
//
//        }
        if ($asJson) json_success(self::$__regs[$name]);
        return self::$__regs[$name];
    }

    static function getAllRegex($asJson = false) {
        if ($asJson) return json_encode(self::$__regs);
        return self::$__regs;
    }

    static function is_preg_error()
    {
        $errors = array(
            PREG_NO_ERROR               => 'Code 0 : No errors',
            PREG_INTERNAL_ERROR         => 'Code 1 : There was an internal PCRE error',
            PREG_BACKTRACK_LIMIT_ERROR  => 'Code 2 : Backtrack limit was exhausted',
            PREG_RECURSION_LIMIT_ERROR  => 'Code 3 : Recursion limit was exhausted',
            PREG_BAD_UTF8_ERROR         => 'Code 4 : The offset didn\'t correspond to the begin of a valid UTF-8 code point',
            PREG_BAD_UTF8_OFFSET_ERROR  => 'Code 5 : Malformed UTF-8 data',
        );
        if (preg_last_error() == PREG_NO_ERROR) return false;
        return $errors[preg_last_error()];
    }
}

if(!function_exists('mime_content_type')) {
    function mime_content_type($filename, $htmlDefault = false) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',

            //font
            'woff2' => 'font/woff2',
        );
        $f = explode('.',$filename);
        $ext = strtolower(end($f));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return $htmlDefault ? 'text/html' : 'application/octet-stream';
        }
    }
}

/**
 * @param $url
 * @param null $data
 * @param array $options
 * @return array|mixed
 * forward_cookie => bool | array of setcookie options
 */
function curl($url, $data = null, $options = []) {
    $options = array_merge(['json' => false, 'method' => null, 'forward_cookie' => false, 'detail' => false, 'header' => [],
        'referer' => null, 'agent' => null], $options);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 40000);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLINFO_HEADER_SIZE, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $options['agent'] ?: $_SERVER['HTTP_USER_AGENT']);

    if (count($options['header'])) curl_setopt($ch, CURLOPT_HTTPHEADER, $options['header']);
    if ($options['referer']) curl_setopt($ch, CURLOPT_REFERER, $options['referer']);
    if ($options['forward_cookie']) {
        $cks = '';
        foreach($_COOKIE as $key=>$val) {
            $cks.="$key=$val;";
        }
        curl_setopt($ch, CURLOPT_COOKIE, $cks);
    }

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    $headers = [];
    $ck = [];
    curl_setopt($ch, CURLOPT_HEADERFUNCTION,
        function($curl, $header) use (&$headers, $options, &$ck)
        {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) return $len; // ignore invalid headers
            //forward set-cookie
            if (strtolower($header[0]) == 'set-cookie') {
                $v1 = explode(';', $header[1]);
                $cook = [];
                foreach ($v1 as $v) {
                    $a = explode('=', trim($v));
                    $cook[$a[0]] = isset($a[1]) ? $a[1] : null;
                    if (isset($a[1]) && $options['forward_cookie']) {
                        $ck_key = trim($a[0]);
                        $ck_val = trim($a[1]);
                        if ($ck_key === 'SameSite' && $options['forward_cookie']['samesite']) {
                            $ck_val = $options['forward_cookie']['samesite'];
                        }
                        setcookie($ck_key, $ck_val, is_array($options['forward_cookie']) ? $options['forward_cookie'] : []);
                    }
                    $ck[$a[0]] = trim($a[1]);
                }
                //$ck[] = $cook;
            }
            $headers[strtolower(trim($header[0]))] = trim($header[1]);
            return $len;
        }
    );

    if(!empty($data)) {
        curl_setopt($ch, CURLOPT_POST, true);
        if ($options['json']) {
            $data_string = json_encode($data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)]);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        } else curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    $res = curl_exec ($ch);

    $info = [
        'status' => curl_getinfo ($ch, CURLINFO_HTTP_CODE),
        'content-type' => curl_getinfo($ch, CURLINFO_CONTENT_TYPE),
        'body' => $res,
        'headers' => $headers,
        'set-cookie' => $ck,
    ];
    if (strpos($info['content-type'], 'application/json') !== false) try {
        $info['body'] = json_decode($info['body'], true);
    } catch (Exception $e) {}

    curl_close ($ch);
    return $options['detail'] ? $info : $info['body'];
}

function curl_post_json($url, $data) {
    return curl($url, $data, ['json' => true]);
}

function curlForward($url, $data = null) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 40000);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLINFO_HEADER_SIZE, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

    // get http header for cookies
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1); //want to echo header?
    if (isset($_COOKIE['gaba_ck'])) {
        curl_setopt($ch, CURLOPT_COOKIE, $_COOKIE['gaba_ck']);
    }

    if(!empty($data)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    $res = curl_exec ($ch);

    //--Body - Header Eject zone--//
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($res, 0, $header_size);
    $body = substr($res, $header_size); //get body out from curl
    curl_close ($ch);

    // check set set cookie
    preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
    if ($matches) {
        setcookie('gaba_ck', explode(strtolower('Set-Cookie: '), $matches[0][0])[1]);
    }

    return $body;
}
