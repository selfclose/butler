<?php

class sqlStatic {

    static protected function has_operator($str) {
        if (is_string($str) && count($str) > 0) {
            return preg_match("/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sCASE|\sTHEN|\sWHEN|\sIN\s*\(|\s)/i", trim($str));
        }
        return true;
    }
    static protected function genCol($col) {
        $str = '';
        $protect_identifiers = true;
        if (is_string($col)) {
            $col = explode(',', $col);
            //return $col.' ';
        }
        if (is_array($col)) {
            foreach ($col as $item) {
                $item = trim(preg_replace("/\s+/", " ", $item));
                $alias = '';

                //should all
                if (preg_match("/\sAS\s/i", $item)) {
                    preg_match_all("/\sAS\s/i", $item, $m);
                    //var_dump($m);
                    $alias_index = strpos($item, $m[0][0]);
                    $alias = ($protect_identifiers ? substr($item, $alias_index,4) . self::escape_identifiers(substr($item, $alias_index + 4)) : substr($item, $alias_index));
                    $item = substr($item, 0, $alias_index);
                }
                else if (strpos($item, " ") !== false) {
                    $alias_index = strpos($item, " ");
                    $alias = ($protect_identifiers && ! self::has_operator(substr($item, $alias_index + 1)) ? ' ' + self::escape_identifiers(substr($item, $alias_index + 1)) : substr($item, $alias_index));
                    $item = substr($item, 0,alias_index);
                }

                //line 233

                $str .= self::escape_identifiers($item) . $alias;
                if( next( $col ) ) {
                    $str.= ', ';
                }
            }
        }
        return $str;
    }

    static protected function escape_identifiers($item = '*') {
        if ($item == '*') return $item;

        if (is_array($item)) {
            foreach ($item as $i) {
                $i = self::escape_identifiers($i);
            }
            return $item;
        }
        elseif ((is_string($item) && preg_match("/^\d+$/", $item)) || $item[0] === "'" || $item[0] === '"' || strpos($item, "(") !== false) {
            return $item;
        }

        $str = '';
        if (strpos($item, ".*") !== false) {
            $str = preg_replace("/`?([^`\.]+)`?\./i", "`$1`.", $item);
        }
        else {
            $str = preg_replace("/`?([^`\.]+)`?(\.)?/i", "`$1`$2`", $item);
        }

        // remove duplicates if the user already included the escape
        return preg_replace("/[`]+/", '`', $str);
    }

    static function query($query) {
        if (!CONFIG['DB_ENABLE']) go500('If you want to use Database, Please enable "DB_ENABLE" in config.php');
        try {
            $res = R::getAll($query);
            Event::emit('sql_query', $query);
            return [
                'success' => true, //(bool)count($res),
                'code' => 'SUCCESS',
                'data' => $res
            ];
        } catch (Exception $e) {
            Event::emit('sql_error', $query);
            return [
                'success' => false,
                'code' => 'QUERY_ERROR',
                'data' => print_r($e)
            ];
        }
    }

    static function insert($table, $data) {
        if (CONFIG['DB_FREEZE']) {
            $k = array_keys($data);
            $v = array_values($data);
            $str = "INSERT INTO `{$table}` (`".implode("`, `", $k)."`)";
            foreach ($v as $key => $item) {
                if (is_array($item)) $v[$key] = json_encode($v, JSON_UNESCAPED_UNICODE);
            }
            $str .= " VALUES('".implode("', '", $v)."')";
            $res = R::exec($str);
        }
        else {
            $r = R::getRedBean()->dispense($table);
            foreach ($data as $k => $v) {
                //if (is_null($v)) continue;
                if (is_array($v)) $v = json_encode($v, JSON_UNESCAPED_UNICODE);
                $r->{$k} = $v;
            }
            $res = R::store($r);
        }
        Event::emit('sql_insert', ['table' => $table, 'data' => $data, 'result' => $res]);
        return ['success' => (bool)$res, 'data' => $res];
    }

    static function update($table, $data, $where) {
        $sql = "UPDATE `".$table."` SET";
        $last = array_key_last($data);
        foreach ($data as $k => $v) {
            $sql .= " `".helper::safe_replace($k)."` = ".(is_null($v) ? 'NULL' : (is_numeric($v) ? helper::safe_replace($v) : "'".$v."'")).($k === $last?' ':',');
        }
        $sql .= self::genWhere($where);
        //die($sql);
        return self::query($sql);
    }

    static function updateOrInsert($table, $data, $where = null) {
        if ($where) {
            $c = self::count($table, '*', $where);
            if ($c['success']) return self::update($table, $data, $where);
        }
        return self::insert($table, $data);
    }

    /**
     * @param $table
     * @param $where
     * @param string $col
     * @param null $limit
     * @param null $order ['age', true]
     * @param null $search ['username', 'MyUs']
     * @return array
     */
    static function find($table, $where = [], $col = '*', $limit = null, $order = null, $search = null)
    {
        $sql = "SELECT " . self::genCol($col) . " FROM `{$table}`";
//        if (is_string($where)) $sql .= $where;
//        elseif (is_array($where)) {
//            if (preg_match('[>=<]', $where[0])) $sql .= $where[0] . ' ' . $where[1];
//            else $sql .= $where[0] . ' = ' . $where[1];
//        }
        $sql .= self::genWhere($where);
        if ($search) $sql .= " AND `".helper::safe_replace($search[0])."` LIKE '%".helper::safe_replace($search[1])."%'";
        if ($order) {
            if (is_array($order)) $sql .= " ORDER BY `".helper::safe_replace($order[0])."` ".helper::safe_replace($order[1])?"DESC":"ASC";
            else $sql .= " ORDER BY ".helper::safe_replace($order);
        }
        if ($limit) $sql .= " LIMIT ".(int)$limit;
        //var_dump($sql);
        return self::query($sql);
    }

    static function findOne($table, $where, $col = '*', $order = null, $search = null) {
        $res = self::find($table, $where, $col, 1, $order, $search);
        if (!count($res['data'])) {
            $res['success'] = false;
            $res['code'] = 'NOT_FOUND';
        }
        if (!$res['success']) return $res;
        $res['data'] = $res['data'][0];
        return $res;
    }

    static function findOneOrInsert($table, $where, $insertData = null) {
        $res = self::findOne($table, $where);
        if ($res['success']) return $res;
        self::insert($table, $insertData);
        return self::findOne($table, $where);
    }

    static function findPaginate($table, $col = '*', $params = [], $join_query = null) {
        $params = array_merge(['page' => 1, 'length' => 100, 'search' => null, 'filter' => null, 'where' => null, 'order_by' => ['id', false], 'group_by' => '', 'custom_total' => null], $params);
        //count
        $where = " WHERE ";
        if (isset($params['where'])) $where .= self::genWhere($params['where'], false);

        if (isset($params['search']) and !is_null($params['search'])) {
            $search = $params['search'];
            if ($where != " WHERE ") $where.= " AND ";
            if (gettype($search) == 'string') {
                $d = self::query("DESCRIBE `".$table."`");
                $last = end($d['data'])['Field'];

                foreach ($d['data'] as $i) {
                    $where.= "`".$i['Field']."` LIKE '%".helper::safe_replace($search)."%'";
                    if ($last !== $i['Field']) $where .= " OR ";
                }
            }
            else {
                if (gettype($search) != 'array') json_error('FILTER_PARAM_INCORRECT');
                $where .= "`".$search[0]."` LIKE '%".$search[1]."%'";
            }
        }

        if (isset($params['filter']) and !is_null($params['filter'])) {
            $filter = $params['filter'];
            //if (gettype($filter) == 'string') $where = ' '.$filter;
            //todo
            $where .= empty($where) ? '' : ' AND '.self::genWhere([$filter[0] => $filter[1]], false);
        }

        if ($where === " WHERE ") $where = "";
        if (isset($params['custom_total'])) $cnt = $params['custom_total'];
        else $cnt = (int)(R::getAll("SELECT COUNT(*) as c FROM `{$table}`{$where}"))[0]['c'];
        $group = (isset($params['group_by']) && strlen($params['group_by'])) ? ' GROUP BY '.$params['group_by'] : '';
        $order = " ORDER BY `{$params['order_by'][0]}` ".($params['order_by'][1]?'DESC':'ASC');
        $page = abs($params['page'])-1;
        $limit = " LIMIT ".($page*(int)$params['length']).",".(int)$params['length'];
        $q = "SELECT ".$col." FROM ".$table.($join_query?' '.$join_query:'').$where.$group.$order.$limit;
        // die($q);
        $r = R::getAll($q);
        return ['success' => true, 'code' => 'SUCCESS', 'data' => $r, 'total' => $cnt, 'page' => $params['page'], 'length' => $params['length'], 'total_page' => $cnt==0||$params['length']==0?0:ceil($cnt/$params['length'])];
    }

    static function delete($table, $where) {
        $sql = "DELETE FROM `".$table."`".self::genWhere($where);
        $res = self::query($sql);
        Event::emit('sql_delete', ['table' => $table, 'where' => $where, 'result' => $res]);
        return $res;
    }

    static protected function genWhere($where, $withWhere = true) {
        if (is_null($where)||!isset($where) || !count($where)) return "";
        $str = $withWhere ? ' WHERE' : '';
        if (!is_array($where)) return $str.$where;
        $last = array_key_last($where);
        $first = array_key_first($where);
        foreach ($where as $k => $v) {
            if (is_null($v)) $v = 'NULL';
            elseif (!isset($v)) continue;
            $ex = explode(' ', $k);
            //if (preg_match('[>=<]', $k))
            $orMode = false;
            if (strtoupper($ex[0]) === 'OR') {
                $orMode = true;
                array_shift($ex);
            }
            $str .= ($k === $first ? '' : ($orMode ? 'OR':'AND'))." `$ex[0]` ".(count($ex)==2?$ex[1]:'=')." ".(($v == 'NULL' || preg_match("/^\-?[0-9]*\.?[0-9]+\z/", $v)) ? helper::safe_replace($v) : "'".helper::safe_replace($v)."'").($k === $last ? '':' ');
        }
        if ($str == ' WHERE') return '';
        return $str;
    }

    static function count($table, $col = '*', $where = null, $returnNumber = false) {
        $str = 'SELECT COUNT('.self::genCol($col).')'.($col=='*'?'AS cnt':'').' FROM `'.$table.'`';
        $str.= self::genWhere($where);
        $res = self::query($str);
        $res['data'] = array_map(function ($o) {return (float)$o;}, $res['data'][0]);
        //for($i=0; $i<count($res['data']); $i++) {$res['data'][$i] = (int)$res['data'][$i];}
        if ($col=='*') $res['data'] = $res['data']['cnt'];
        if (!$res['data']) {
            $res['success'] = false;
            $res['code'] = 'NOT_FOUND';
        }
        return $returnNumber ? $res['data'] : $res;
    }

    static function countUp($by = 1) {
        return 'update result set cnt = 0';
    }

    protected function countDown($by) {

    }

    static function increment($table, $where, $col, $by = 1) {
        $by = abs($by);
        return self::query("UPDATE `$table` SET `$col` = `$col` + $by".self::genWhere($where));
    }

    static function decrement($table, $where, $col, $by = 1) {
        $by = abs($by);
        return self::query("UPDATE `$table` SET `$col` = `$col` - $by".self::genWhere($where));
    }

    static function sum($table, $col = '*', $where, $returnNumber = false) {
        $sql = "SELECT SUM(".self::genCol($col).") AS sm FROM `".$table."`".self::genWhere($where);
        $res = self::query($sql);
        return $returnNumber ? (float)$res['data'][0]['sm'] : $res;
    }

    //----ETC----//
    protected function isTableExist($tableName) {

    }

    protected function resetIndex() {

    }

    static function findFromDateOffset($table, $where = null, $dateOffset = 0, $column = 'created_at') {
        return self::query("SELECT * FROM `$table` ".self::genWhere($where)." AND DATE(`$column`) = CURDATE() + INTERVAL $dateOffset DAY");
    }
}
