<?php
set_error_handler(function( $errno, $errstr, $errfile, $errline, $context)
{
    Event::emit('error', debug_backtrace());
    $err = "$errstr $errfile:$errline ".(is_array($context)?json_encode($context):$context);
//    $err = ['type' => $errstr, 'file' => $errfile, 'line' => $errline, 'context' => $errcontext];
    //Event::emit('ERROR', ['ERR_NO' => $errno, 'ERR_STR' => $errstr, 'ERR_FILE' => $errfile])
//    echo 'Into '.__FUNCTION__.'() at line '.__LINE__.
//        "\n\n---ERRNO---\n". print_r( $errno, true).
//        "\n\n---ERRSTR---\n". print_r( $errstr, true).
//        "\n\n---ERRFILE---\n". print_r( $errfile, true).
//        "\n\n---ERRLINE---\n". print_r( $errline, true).
//        "\n\n---ERRCONTEXT---\n".print_r( $errcontext, true).
//        "\n\nBacktrace of errorHandler()\n".
    if (!isset(CONFIG['Debug'])) return;
    //debug_backtrace()
    if (CONFIG['Debug']['File']) _log($err, null,'ERROR');
});

function console($log_msg, $prefix = null, $type = 'INFO')
{
    if (!isset(CONFIG['Debug'])) return;
    $folder = __DIR__."/../../tmp";
    if (!file_exists($folder)) mkdir($folder, 0777, true);
    if (is_array($log_msg) || is_object($log_msg)) $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
    $logFile = $folder.'/console.log';
    file_put_contents($logFile, ymd().' | '.$type.' | '.(is_null($prefix)?'':$prefix.': ').$log_msg . "\n", FILE_APPEND);

    //create last line
//    set_time_limit(300);
//    ini_set('memory_limit', '-1');
    $arr = file($logFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $lines = ( false === $arr) ? 0 : count($arr);
//    set_time_limit(30);// restore to default
//    ini_set('memory_limit','128M');// restore to default
    file_put_contents($folder.'/flag.tmp', $lines);

    return true;
}


function _log($log_msg, $prefix = null, $type = 'INFO') {
    
}

if (CONFIG['Debug']['HTTP']) {
    $_ROUTE->get('/debugger', function () {
        _sysRender(__DIR__.'/../file/debug.php');
    });

    $_ROUTE->get('/assets/_sys/debug-sse', function () {
        include (__DIR__.'/../file/debug-sse.php'); //debug page event
    });
    
    $_ROUTE->post('/debugger', function () {
        $folder = __DIR__."/../../tmp";
        $file = $folder.'/console.log';
        $post = POST();
        switch ($post['action']) {
            case 'clear':
                if (!file_exists($file)) json_error('NO_CONSOLE_FILE');
                file_put_contents($file, '');
                unlink($folder.'/flag.tmp');
//                file_put_contents($folder.'/flag.tmp', '0');
                json_success('CLEARED');
                break;
            default: json_error('ACTION_ERROR');
        }
    });
}