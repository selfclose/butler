<?php
namespace selfclose\Butler\Controller;
use \R as R;
use selfclose\Butler\sql;

class Setting extends sql {
    static $table = 'setting';

    public function addSetting($data) {
        if (!isset($data['name'])) json_error('MORE_FIELD_REQUIRED');
        if (R::count(self::$table, 'name = ?', [$data['name']])) json_error('SETTING_DUPLICATE');
        $r = R::dispense(self::$table);
        $r->name = $data['name'];
        $r->type = gettype($data['val']);
        if ($r->type == 'integer' or $r->type == 'double') $r->type = 'number';
        else if ($r->type == 'array' or $r->type == 'object') {
            $r->type = 'object';
            $data['val'] = json_encode($data['val']);
        }
        $r->val = $data['val'];
        return R::store($r);
    }

    public function getSetting($data, $full = false)
    {
        if (is_string($data)) $data = ['name' => $data];
        if (!isset($data['name'])) json_error('MORE_FIELD_REQUIRED');
        $r = $this->findOne(['name' => $data['name']]);
        if (!$r['success']) return json_error('SETTING_NOT_EXIST');
        $r = $this->convert($r['data']);
        return $full ? $r : $r['val'];
    }

    public function setSetting($data)
    {
        if (!isset($data['name']) or !isset($data['val'])) json_error('MORE_PARAM_REQUIRED');
        if ($r = R::findOne(self::$table, ' name = ?', [$data['name']])) {
            if (gettype($data['val']) != $r['type']) json_error('TYPE_NOT_MATCH_WITH_OLD');
            $r->val = $data['val'];
            return R::store($r);
        }
        json_error('SETTING_NOT_EXIST');
    }

    private function convert($data, $onlyVal = false)
    {
        if ($data['type'] == 'number') $data['val'] = (float)$data['val'];
        elseif ($data['type'] == 'object') $data['val'] = json_decode($data['val'], false, 512, JSON_UNESCAPED_UNICODE);
        return $onlyVal ? $data['val'] : $data;
    }

    public function getAllSettings() {
        $r = R::find(self::$table);
        if (!$r) {
            return json_error('SETTING_NOT_EXIST');
        }
        $col = [];
        $r = R::exportAll($r);
        for ($i=0;$i<count($r);$i++) {
            $col[$r[$i]['name']] = $this->convert($r[$i], true);
        }
        return $col;
    }
}
