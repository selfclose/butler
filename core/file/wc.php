<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Butler - Welcome</title>
    <link rel="stylesheet" href="/assets/_sys/style.css">
</head>
<body>
<div class="panel fx">
    <div>
        <img class="logo" src="logo.png">
    </div>
    <div class="w-100">
        <p>Butler Simplest framework</p>
        <h3>Welcome to Butler Simplest framework</h3>
        <p>Version <?=BUTLER_VER?></p>
        <ul>
            <li>Please duplicate config.default.php To config.php, And modify file</li>
        </ul>
    </div>
</div>
</body>
</html>
