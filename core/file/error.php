<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Butler - Error</title>
    <base href="/assets/_sys/">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="panel">
    <div class="fx">
        <div>
            <img class="logo" src="logo.png">
        </div>
        <div class="w-100">
            <p>Butler Simplest framework</p>

        </div>
    </div>
    <h3>❗️ERROR</h3>
    <pre class="panel" style="white-space: pre-wrap;"><?=($GLOBALS['theme']['txt'])?></pre>
</div>
</body>
</html>
