<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Butler - Debugger</title>
    <base href="/assets/_sys/">
    <link href="style.css" rel="stylesheet"/>
    <link rel="stylesheet" href="style.css">
</head>
<style>
    #out {
        height: calc(100vh - 14em);
        overflow-y: scroll;
        font-family: monospace;
        font-size: 12px;
        line-height: 15px;
        background: #000;
        padding: 1em;
    }
    .ts {
        color: darkgoldenrod;
    }
    .ERROR {
        color: red;
    }
    .INFO {
        color: aqua;
    }
</style>
<body>
<div style="position: absolute; right: 0; top: 0;"><button class="btn" id="btn-how">?</button></div>
<div class="panel">
    <div class="fx">
        <div>
            <img class="logo" src="logo.png" style="width: 64px; height: 64px;">
        </div>
        <div>
            <p>Butler Simplest framework : Debugger</p>
            <div><button class="btn" id="btn-clear">Clear</button></div>
        </div>
    </div>
    <div class="w-100" style="margin-top:0.5em;">
        <div id="out"></div>
    </div>

</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    String.prototype.escape = function() {
        var tagsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };
        return this.replace(/[&<>]/g, function(tag) {
            return tagsToReplace[tag] || tag;
        });
    };
    let term, readLoop = true, lastLine = 0, autoScroll = true;
    $(function() {
        //check for browser support
        if(typeof(EventSource)!=="undefined") {
            var eSource = new EventSource("/assets/_sys/debug-sse");
            // var eSource = new EventSource("/debug-sse.php");
            eSource.onerror = function(e) {
                setTimeout(()=>location.reload(), 5000);
            };
            eSource.onmessage = function(event) {
                //if (event.lastEventId === 'ADD_LINE') {
                $('#out').append('<p>'+event.data.escape().replace(/^([\s\d\:\-]+)\|\s(ERROR|INFO)\s\|/, '<span class="ts">$1</span> <span class="$2">$2</span>')+'</p>');
                //.replace(/(:\d+\s)/, '<span class="line">$1</span>')
                if (autoScroll) $('#out').scrollTop($('#out')[0].scrollHeight - $('#out')[0].clientHeight);
                while ($('#out > p').length > 1000){
                    $('#out > p:first').remove();
                }
                //}
            };

            $('#btn-clear').on('click', ()=>{
                $.post('/debugger', {action: 'clear'}).then(()=>$('#out > p').remove());
            });
        } else {
            $('#out').html('<p>Sorry, Your Browser not support EventSource, Please consider use other browser!</p>');
        }

        $('#btn-how').on('click', ()=>{
            $('#out').append('<p>How to use: This terminal will debug when using "console(content, title)" Or An error occure.</p>');
        });
    });


</script>
</html>
