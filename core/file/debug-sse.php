<?php
$accept = getallheaders()['accept'] ?: getallheaders()['Accept'];
if ($accept !== 'text/event-stream') die('This file is for debugger only.');
set_time_limit(0);
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

/**
 * Constructs the SSE data format and flushes that data to the client.
 *
 * @param string $id Timestamp/id of this connection.
 * @param string $msg Line of text that should be transmitted.
 */
function sendMsg($id, $msg) {
    echo "id: $id" . PHP_EOL;
    echo "data: $msg" . PHP_EOL;
    echo "retry: 999999" . PHP_EOL;
    echo PHP_EOL;
    ob_end_flush();
    flush();
}

$lastLine = '0';

$folder = __DIR__."/../../tmp";
$logFile = $folder.'/console.log';
if (!file_exists($logFile) || file_get_contents($folder.'/flag.tmp') == '0') console('BUTLER debugger '.BUTLER_VER);
while (true) {
    if (connection_aborted()) {
        console('USER GONE');
        exit();
    }
    $newLine = file_get_contents($folder.'/flag.tmp');
    if ($lastLine == $newLine) continue;
    //new update read new line
    $arr = file($logFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $arrLine = array_slice($arr, $lastLine);
    $lastLine = $newLine;
    foreach($arrLine as $l)
    {
        sendMsg('ADD_LINE', $l);
    }
    sleep(1);
}
