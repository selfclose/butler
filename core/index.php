<?php
/**
 * Butler PHP Framework v1.3
 * Create By: Arnanthachai Chomphuchai
 * Email: seflclose@gmail.com
 *
 * [ Simple But Useful ]
 */

if (!defined('__ROOT__')) define('__ROOT__', $_SERVER['DOCUMENT_ROOT']); //die('Please Copy "selfclose/butler/index.php" To your Root Folder');

call_user_func(function(){
    $f = __ROOT__.'/config.php';
    if (!file_exists($f)) die(include __DIR__.'/file/wc.php');
    require_once $f;
});

if (!defined('CONFIG')) die('CONFIG Error');

$GLOBALS['$__theme'] = (object)[
    '_css_end' => false,
    'css' => [],
    '_script_end' => false,
    'script' => []
];

//call_user_func(function() {
//    $src = '';
//    if (is_array(CONFIG['SRC'])) {
//        //Map domain mode
//        $src =
//            //check incoming is port or sub-domain
//        $subdomain = substr_count($_SERVER['HTTP_HOST'], '.') > 1 ? substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'], '.')) : '';
//        if ($subdomain && $subdomain != 'www' && ($_SERVER['SERVER_PORT'] == 80 || $_SERVER['SERVER_PORT'] == 443)) {
//            $src = CONFIG['SRC'][$subdomain];
//        } else {
//            //port
//            $src = CONFIG['SRC'][$_SERVER['SERVER_PORT']];
//        }
//    }
//    else {
//        $src = CONFIG['SRC'];
//    }
//    define('BUNDLE_SRC', $src);
//});

//check is config mapping server name or just string of folder
define('BUNDLE_SRC', is_array(CONFIG['SRC']) ? isset(CONFIG['SRC'][$_SERVER['HTTP_HOST']]) ? CONFIG['SRC'][$_SERVER['HTTP_HOST']]['src'] : die('Origin "'.$_SERVER['HTTP_HOST'].'" Not in config.php') : CONFIG['SRC']);
//define('BUNDLE_SRC', is_array(CONFIG['SRC']) ? CONFIG['SRC'][$_SERVER['SERVER_PORT']] : CONFIG['SRC']);
define('__BUNDLE__', __ROOT__.'/src/'.BUNDLE_SRC);
define('__ASSETS__', '/assets/'.BUNDLE_SRC); //assets path that rewrite by .htaccess file

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (CONFIG['Timezone']) date_default_timezone_set(CONFIG['Timezone']);

//if (!preg_match('/^\/assets\//', $_SERVER['REQUEST_URI']))
session_start(); //assets no need session

//if (getallheaders()['Accept'] !== 'text/event-stream')

function _sysRender($file, $data = null) {
    if (!file_exists($file)) die('Cannot find template path: '.$file);
    global $_ROUTER;
    if (is_array($data)) $GLOBALS['theme'] = array_merge($data);
    include $file;
    return die(200);
}

//----- Printing function -----//
function jsonAutoInt($json) {
    return json_decode(json_encode( $json, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK ));
}

function json($jsonObj, $autoFormat = false) {
    header('Content-Type: application/json');
    return die($autoFormat ? jsonAutoInt($jsonObj) : json_encode($jsonObj,  JSON_UNESCAPED_UNICODE));
}

function json_error($code = 'ERROR', $data = null, $autoFormat = false) {
    header('Content-Type: application/json');
    return die(json_encode(['success' => false, 'code' => $code, 'data' => $data], JSON_UNESCAPED_UNICODE));
}
function json_success($data = null, $code = 'SUCCESS') {
    header('Content-Type: application/json');
    return die(json_encode(['success' => true, 'code' => $code, 'data' => $data], JSON_UNESCAPED_UNICODE));
}

// -- Other Global Var
function HOST($full_url = false) {
    $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'];
    if ($full_url) $link .= $_SERVER['REQUEST_URI'];
    return $link;
}
function redirect($url) {
    header("Location: ".$url);
    die();
}
/**
 * Handle AngularJS $_REQUEST -> $_POST
 * @return mixed
 */
function POST($specific = null) {
    $ret = null;
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty( $_POST )) {
        $rq = json_decode(file_get_contents('php://input'), true);
        return $specific ? (isset($rq[$specific]) ? $rq[$specific] : null) : $rq;
    }
    return $specific ? (isset($_POST[$specific]) ? $_POST[$specific] : null) : $_POST;
}

function sid() {
    if (!isset($_COOKIE['PHPSESSID'])) return null;
    return $_COOKIE['PHPSESSID'];
}

function ymd($noTime = false) {
    return date('Y-m-d' . ($noTime ? '' : ' H:i:s'));
}

require_once __DIR__.'/event.php';
require_once __DIR__.'/inc/sql-static.php';
require_once __DIR__.'/inc/sql.php';
require_once __DIR__.'/template.php';
require_once __DIR__.'/inc/helper.php';
require_once __DIR__.'/inc/debug.php';
require_once __DIR__.'/log.php';

if (!file_exists(__BUNDLE__)) errorPage('SRC Folder not exist, "src/'.BUNDLE_SRC);

//----- DATABASE -----//
require_once __DIR__.'/vendor/RedBeanPHP5_3_1/rb.php';
if (CONFIG['DB_ENABLE']) {
    if (isset(CONFIG['DB_SQLITE'])) {
        R::setup( 'sqlite:'.CONFIG['DB_SQLITE'] );
    } else {
        R::setup('mysql:host=' . CONFIG['DB_HOST'] . ':' . CONFIG['DB_PORT'] . ';dbname=' . CONFIG['DB_NAME'], CONFIG['DB_USER'], CONFIG['DB_PASS']);
    }
    if (!R::testConnection()) errorPage('Cannot connect to DB, Please check config for database configuration.');
    R::freeze(CONFIG['DB_FREEZE']);
    R::debug(CONFIG['DB_DEBUG']);
}

if (!file_exists(__BUNDLE__.'/route.php')) {
    errorPage('Route File required in, '.__BUNDLE__.'/route.php');
}
//require_once __DIR__.'/autoload.php';
require_once __BUNDLE__.'/route.php';
//require_once __DIR__.'/vendor/Phroute/RouteDataProviderInterface.php';
//require_once __DIR__.'/vendor/Phroute/RouteParser.php';
//require_once __DIR__.'/vendor/Phroute/Route.php';
//require_once __DIR__.'/vendor/Phroute/RouteCollector.php';
//$router = new \Phroute\Phroute\RouteCollector();
//$router->get('/', function () {
//    die('<h1>gfgfdgfg</h1>');
//});
//$_ROUTER = new \Bramus\Router\Router();
////$_ROUTER->setBasePath(__BUNDLE__.'/view');
//require_once __DIR__.'/routing.php';
//require_once __DIR__.'/route.php';
