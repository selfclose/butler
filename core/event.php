<?php
class Event {
    private static $events = [];
    public static function on($name, $callback) {
        self::$events[$name][] = $callback;
    }
    public static function emit($name, $argument = null) {
        if (!isset(self::$events[$name])) return null;
        foreach (self::$events[$name] as $event => $callback) {
            if($argument) call_user_func($callback, $argument);
            else call_user_func($callback);
        }
    }
}
