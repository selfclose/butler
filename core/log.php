<?php
/**
 * Class Log
 * Usage:
 * $log = new Log('/tmp/%Y-%m-%d', 'log-%H-%m');
 * $log->log('Hello');
 */

class Log {
    protected $folder;
    protected $filename;

    /**
     * Log constructor.
     * @param null $folder
     * @param string $fileDateFormat strftime
     * @param bool $rotate
     */
    public function __construct($folder = null, $fileDateFormat = '%Y-%m-%d %H', $rotate = false)
    {
        if (!$folder) $this->folder = __ROOT__;
        else $this->folder = substr($folder, 0, 1) === '/' ? (__ROOT__.strftime($folder)) : strftime($folder);
        $this->filename = strftime($fileDateFormat ?: '%Y-%m-%d %H').'.log';
        if (!file_exists($this->folder)) mkdir($this->folder, 0777, true);
    }

    function log($log_msg, $prefix = null, $type = 'INFO') {
        if (is_array($log_msg) || is_object($log_msg)) $log_msg = json_encode($log_msg, JSON_UNESCAPED_UNICODE);
        $logFile = $this->folder.'/'.$this->filename;
        file_put_contents($logFile, date('Y-m-d H:i:s').' | '.$type.' | '.(is_null($prefix)?'':$prefix.': ').$log_msg . "\n", FILE_APPEND);
    }
}
