<?php
//----- theme function -----//
function add_script($url, $printNow = false, $defer = false) {
    if ($printNow) {
        echo '<script type="text/javascript" src="'.$url.'"></script>';
        return;
    }
    return array_push($GLOBALS['$__theme']->script, $url);
}
function add_css($url, $printNow = false) {
    if ($printNow) {
        echo '<link rel="stylesheet" href="'.$url.'">';
        return;
    }
    return array_push($GLOBALS['$__theme']->css, $url);
}
function the_header() {
    //add something first
    ?>

    <?php
    //echo '<base href="'.__ASSETS__.'/">';
    Event::emit('before_css');
    $__theme = $GLOBALS['$__theme'];
    foreach ($__theme->css as $i) {
        add_css($i, true);
    }
    $__theme->_css_end = true;
    Event::emit('after_css');
}
function the_footer() {
    $__theme = $GLOBALS['$__theme'];
    Event::emit('before_script');
    foreach ($__theme->script as $i) {add_script($i, true);}
    $__theme->_css_end = true;
    Event::emit('after_script');
}
function the_body($file) {
    if (!file_exists($file)) throw new Error('File Not Exist');
    $f = file_get_contents($file);
    echo $f;
}

function render($file, $data = null) {
    Event::emit('before_render');
    $file = __BUNDLE__.'/View/'.$file;
    if (!file_exists($file)) go404('Cannot find template path: '.$file);
    global $__render_var;
    if (!isset($GLOBALS['theme'])) $GLOBALS['theme'] = [];
    if (is_array($data)) $GLOBALS['theme'] = array_merge($GLOBALS['theme'], $data);
    unset($data);
    global $_ROUTER;
    header('Content-Type: text/html; charset=utf-8');
    include $file;

    $caller = debug_backtrace()[1]['function'];
    if ($caller == 'get_header' || $caller == 'get_footer') return true;
    return die(200);
}

function get_header($title = '', $file = 'header.php') {
    render($file, ['title' => $title]);
}

function get_footer($file = 'footer.php') {
    render($file);
}

function errorPage($text) {
    _sysRender(__DIR__.'/file/error.php', ['txt' => $text]);
}
function go404($custom_text = null) {
    header('HTTP/1.1 404 Not Found');
    $f = __BUNDLE__.'/View/404.php';
    if (file_exists($f)) render('404.php', ['url' => HOST(true)]);
    errorPage($custom_text ?: '404 Not found: '.HOST(true));
}
function go500($custom_text = null) {
    header('HTTP/1.1 500 Internal Server Error');
    $f = __BUNDLE__.'/View/500.php';
    if (file_exists($f)) render('500.php', ['url' => HOST(true), 'text' => $custom_text]);
    errorPage($custom_text ?: '500 Internal Error: '.HOST(true));
}

function sendFile($file) {
    if (!file_exists($file)) return false;
    header('Content-Description: File Transfer');
    header("Content-type: ".mime_content_type($file)."; charset: UTF-8");
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: '.filesize($file));
    readfile($file);
}
