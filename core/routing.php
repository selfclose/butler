<?php
/**
 * Routing that always bother me
 */

function autoload($className)
{
//    json(debug_backtrace()[1]);

    $className = ltrim($className, '\\');
    //exception for system Controller, Don't get inside src
//    if (in_array($className, ['mysql', 'R'])) {
//
//    }
    if (preg_match('/^selfcloser\\\.*$/', $className)) {
        $fileName = __DIR__.'/';
    }
    else $fileName = __ROOT__.'/src/';

    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    $fileName  = str_replace('\\', '/', $fileName);
    if (strpos($className, 'Model_', 0) === 0) return; //damn redbean try to load model

    if (!file_exists($fileName)) {
        json_error('FILE_NOT_EXIST', debug_backtrace());
    }
    require $fileName;
}
spl_autoload_register('autoload');

// Register Bundle's route
if (!file_exists(__BUNDLE__.'/Route.php')) _sysRender(__DIR__.'/file/error.php', ['txt' => 'Warning! Bundle Route.php not exist!']);
include __BUNDLE__.'/Route.php';

// Register Our api route

function getURL($url){
    $url_parts = parse_url($url);
    $constructed_url = $url_parts['scheme'] . '://' . $url_parts['host'] . (isset($url_parts['path'])?$url_parts['path']:'');

    return $constructed_url;
}

/**
 * apis is point direct to Controller Class, It's call only public function
 * PS. All public function no need to return, Can be use json function inside, Simple
 */
$_ROUTER->post('/apis/([\w]+)/([\w]+)', function ($class, $method) {
//    $f = __BUNDLE__.'/Controller/'.$class.'.php';
//    if (!file_exists($f)) json_error('CONTROLLER_MODEL_OR_METHOD_NOT_EXIST', ['path' => $f]);
//    if (is_file($class)) require_once $f; else require_once strtolower($f);
//    if (!class_exists($class)) json_error('CLASS_NOT_EXIST', ['class' => $class]);
//
    $class = BUNDLE_SRC.'\\Controller\\'.$class;
    $ctrl = new $class();
    if (!method_exists($ctrl, $method)) json_error('METHOD_NOT_EXIST', ['class' => $class, 'method' => $method]);
    $reflection = new ReflectionMethod($ctrl, $method);
    if (!$reflection->isPublic()) json_error("METHOD_IS_NOT_PUBLIC", ['class' => $class, 'method' => $method]);
    if ($reflection->isConstructor()) json_error("DO_NOT_CALL_INSTRUCTOR");
    /**
     * Run the command
     */
    // inside public method can call any public function of framework
    $ret = $ctrl->$method(POST());
    if (!$ret) json_error('API_METHOD_ERROR');
    if (isset($ret['success'])) json($ret);
    json_success($ret);
    die(200);
});

//$_ROUTER->get('/apis/([\w]+)/([\w]+)', function ($class, $method) {
//    $f = __BUNDLE__.'/Controller/'.$class.'.php';
//    if (!file_exists($f)) json_error('CONTROLLER_MODEL_OR_METHOD_NOT_EXIST', ['path' => $f]);
//    if (is_file($class)) require_once $f; else require_once strtolower($f);
//    if (!class_exists($class)) json_error('CLASS_NOT_EXIST', ['class' => $class]);
//
//    $ctrl = new $class();
//    if (!method_exists($ctrl, $method)) json_error('METHOD_NOT_EXIST', ['class' => $class, 'method' => $method]);
//    $reflection = new ReflectionMethod($ctrl, $method);
//    if (!$reflection->isPublic()) json_error("METHOD_IS_NOT_PUBLIC", ['class' => $class, 'method' => $method]);
//    if ($reflection->isConstructor()) json_error("DO_NOT_CALL_INSTRUCTOR");
//    /**
//     * Run the command
//     */
//    // inside public method can call any public function of framework
//    $ctrl->$method();
//    die(200);
//});


$_ROUTER->set404(function() {
    go404();
});
$_ROUTER->run();

// << This is the end of program
