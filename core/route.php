<?php
$dispatcher =  new Phroute\Phroute\Dispatcher($GLOBALS['$_ROUTE']->getData());
$response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

// Print out the value returned from the dispatched function
echo $response;