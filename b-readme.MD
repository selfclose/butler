## Butler PHP, Simple But Useful PHP Framework

**!! I wrote this for personal use, But You can use too !!**

Always do the same every project. Routing, Mysql, Captcha, User. So I build this one

* Bundle For Multiple Project
* Light Framework (Around 6 MB)
* Routing (Require from: phroute)
* Dynamic API Path, Call (public) method, No need to map route, No dependency injection
* Simple Mysql function (Wrap over Redbean For Automatic create table and column
* Captcha
* Simple Template Engine

### Simple Structure
```text
└── src
   └── MyBundle
       ├── Controller
       ├── Model
       ├── View
       ┆  ├─ footer.php
       ┆  ├─ header.php
       ┆  ├─ index.php
       ┆  └─ 404.php (Optional)
       └── route.php
```

### Installation
'composer.json' file must have this
```json
{
    "require": {
        "selfclose/butler": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://selfclose@bitbucket.org/selfclose/butler.git"
        }
    ]
}
```
Next Copy this 2 files to project root folder
* config.php
* .htaccess

### Simple API Call
`localhost:80/apis/{controller}/{public method}`

### Simple Usage
We have few rule but most durable
```php
require(
```

keep data
### Global constant

#### -- Path --
- \_\_ROOT\_\_  Root path of project
- \_\_BUNDLE\_\_ Path of current bundle project you working on
- \_\_ASSETS\_\_ Path of current bundle project (For script or style)

#### -- Variable --
$_ROUTE  create routing in project
CONFIG  Calling config inside config.php file

#### -- URI --
HOST  Get link of request, get Full link if argument is true
POST  Same as $_POST But all good for AngularJS for grab $http()

$GLOBAL['theme'] Appear when render function was call, Use while inside /view file

### API
You can call api use post request
`/apis/MY_CONTROLLER/MY_METHOD`

It going to read

`/src/{Your SRC config}/controller/{MY_CONTROLLER}.php`

And Your method will execute inside class

**Ps. Only "Public method" going to execute, Don't worry**

#### Functions
* json for return JSON format
* json_error Return json format with success false and code
* json_success Return json format with success true and code


### Framework namespace
"Butler"

* mysql
`class MyClass extend selfclose\Butler\sql {}`

### Event
For declare or hook when certain event that happen, You can custom event too.
Methods:
* emit(event, param)
* on(event, param)

**System event**
- 'before_render' Fire inside render() function before start render
- 'before_css'
- 'after_css'
- 'before_script' Before start writing script tag at footer
- 'after_script' After last script in footer, This is useful if you want to add extra \<script\> tag of the page.
- 'sql_query'
- 'sql_insert'
- 'sql_update'
- 'sql_delete'
- 'error'

[Graph Read here](https://krispo.github.io/angular-nvd3/#/lineChart)
[Route](https://github.com/bramus/router)
```javascript




if (isset($_GET['api'])) {
    require_once __DIR__.'/../vendor/RedBeanPHP5_3_1/rb.php';
    require_once __DIR__.'/../vendor/notorm/NotORM.php';

    R::setup('sqlite:db.db', null, null, true);
    if (!R::testConnection()) die('Cannot connect to DB');
    //R::fancyDebug(true);

    switch ($_GET['api']) {
        case 'poll':
            if (!isset($_GET['id'])) json_error('REQUIRED_MORE');
            $id = $_GET['id'];
            $poll = R::findOne('poll', 'id = ?', [$id]);
            if (!$poll) json_error('POLL_NOT_EXIST');
            $choicd = R::find('choice', 'poll_id = ?', [$id]);
            json(['title' => $poll, 'choice' => $choicd]);
            break;
        case 'test':
            $pdo = new PDO('sqlite:db.db', null, null, [
                PDO::ATTR_STRINGIFY_FETCHES => false,
                PDO::ATTR_EMULATE_PREPARES => false,
            ]);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
            $res = $pdo->query('SELECT choice.* FROM `choice`')->fetchAll(PDO::FETCH_ASSOC);
            //$db = new NotORM($e);
//            $res = $db->application()->select('*');
            var_dump($res);
//            var_dump(json_decode(json_encode( $res, JSON_NUMERIC_CHECK )));
            break;
        case 'session':
            json($_COOKIE['PHPSESSID']);
            break;
    }
}
```

การทำงานคร่าวๆ

Enable sparse-checkout:

git config core.sparsecheckout true
Configure sparse-checkout by listing your desired sub-trees in .git/info/sparse-checkout:

`echo some/dir/ >> .git/info/sparse-checkout`

`echo another/sub/tree >> .git/info/sparse-checkout`

`git read-tree -mu HEAD`

## Future
* รองรับ Route จาก subdomain


### ไฟล์ Config.php

`"SRC" => string | array,`

ใช้เรียกโฟลเดอร์โปรเจค ได้ 2 แบบ
1. เรียกโฟลเดอร์ `"SRC" => "โฟลเดอร์โปรเจค"`
2. Server name Mapping `$_SERVER['HTTP_HOST']`
```php
"SRC" => [
    'localhost' => 'project1',
    'mysite.com' => 'project2',
    'ads.mysite.com' => 'project3',
    'localhost:8082' => 'project4'
]
```
**หมายเหตุ:** หาก Map ตามข้อ 2 ให้ Apache หรือ NginX ชี้มาที่โฟลเดอร์ Root ทั้งหมด


### Static Class
Class เหล่านี้จะช่วยให้งานของท่านง่ายขึ้น
- password
    - encrypt(password) เข้ารหัส password
    - compare(origin_pass, compare_pass) ใช้เทียบว่ารหัสตรงกัน origin_pass จะเป็นรหัสจาก encrypt() ส่วน compare_pass คือ string ที่ยังไม่ได้เข้ารหัส
- regex
    - addRegex() เพิ่ม regex เพื่อนำไปตรวจสอบภายหลัง
    ```regex::addRegex('username', '/^[A-Za-z0-9-_]{6,20}$/', 'ชื่อผู้ใช้จะต้องเป็น A-Z หรือเลข ยาว 6 - 20 ตัว', 20);```
    - checkRegex() เปรียบเทียบ สามารถ json กลับออกสู่ client ได้โดยไม่จำเป็นต้องเขียนเชค
    



### Function
curl(url, post_data, options)

สำหรับ curl สามารถ post
- url:String Url ที่จะ Request
- post_data:Array Default:null หากส่ง data จะเปลี่ยนเป็น POST อัตโนมัติ
- options:Array สำหรับปรับแต่งฟังชั่น
    - json:Boolean Default:false ให้ POST ไปแบบ json (ปกติจะเป็น form data)
    - detail:Boolean Default:false ให้ Return พร้อมรายละเอียด เช่น status, header, body
    - forward_cookie:Boolean Default:false จะทำการ forward set-cookie ส่งต่อให้ client
    
curl_post_json(url, post_data)

คำสั่งเดียวกันกับ curl แต่จะปรับ options->json = true

