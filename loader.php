<?php
if (defined('BUTLER_VER')) die ('Error: Loader must init once.');
define('BUTLER_VER', '1.2.2');
define('__HOME__', __DIR__);
if (PHP_VERSION_ID < 70300) die('Butler Framework Require PHP >= 7.3');
$_ROUTE = new Phroute\Phroute\RouteCollector();

require 'core/index.php';

$_ROUTE->get('/assets/_sys/{file:[\w\/\.]+}', function ($file) {
    $file = __DIR__.'/core/file/'.$file;
    if (!file_exists($file)) return go404();
    header('Content-Description: File Transfer');
    header("Content-type: ".mime_content_type($file)."; charset: UTF-8");
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: '.filesize($file));
    readfile($file);
});

$dispatcher =  new Phroute\Phroute\Dispatcher($_ROUTE->getData());
try {
    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    echo ($response);
} catch (Exception $e) {
    go500($e);
    //die($e->getMessage());
}
